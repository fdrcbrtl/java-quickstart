package ar.edu.untref.aydoo;

import org.junit.Test;
import org.junit.Assert;

public class CalculadoraTest 
{
    @Test
    public void sumaDosMasTresYEsCorrecto()
    {
        Calculadora calculadora = new Calculadora();
        Integer resultado = calculadora.sumar(2, 3);
        Assert.assertEquals(new Integer(5), resultado);
    }
    
    @Test
    public void sumaTresMasTresYEsCorrecto()
    {
        Calculadora calculadora = new Calculadora();
        Integer resultado = calculadora.sumar(3, 3);
        Assert.assertEquals(new Integer(6), resultado);
    }   
}
