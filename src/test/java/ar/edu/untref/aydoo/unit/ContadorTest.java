package ar.edu.untref.aydoo;

import org.junit.Test;
import org.junit.Assert;
import java.util.HashMap;
import java.util.Map;

public class ContadorTest 
{
    @Test
    public void contarCaracteresCadenaVacia()
    {
        String cadenaVacia = "";
        Contador contador = new Contador();
        HashMap<Character,Integer> mapaCaracteres = contador.contarCaracteres(cadenaVacia);
        Assert.assertEquals(0, mapaCaracteres.size());
    }
    
    @Test
    public void contarCaracteresUnaLetra()
    {
        String cadena = "m";
        Contador contador = new Contador();
        HashMap<Character,Integer> mapaCaracteres = contador.contarCaracteres(cadena);
	int cantidadLetraM = mapaCaracteres.get('m');
        Assert.assertEquals(1, cantidadLetraM);
    }
}
