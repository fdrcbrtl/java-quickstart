package ar.edu.untref.aydoo;

import org.junit.Test;
import org.junit.Assert;

public class FooTest 
{
    @Test
    public void doFooShouldReturnFoo()
    {
        Foo foo = new Foo();
        String result = foo.doFoo();
        Assert.assertEquals("foo", result);
    }

    @Test
    public void stringAMayuscula()
    {
        String nombreEnMinuscula = "nombre";
        String nombreEnMayuscula = nombreEnMinuscula.toUpperCase();
        Assert.assertEquals("NOMBRE", nombreEnMayuscula);
    }

    @Test
    public void stringAMinuscula()
    {
        String nombreEnMayuscula = "NOMBRE";
        String nombreEnMinuscula = nombreEnMayuscula.toLowerCase();
        Assert.assertEquals("nombre", nombreEnMinuscula);
    }

    @Test
    public void stringLength()
    {
        String cadena = "12345678";
        int tamanio = cadena.length();
        Assert.assertEquals(tamanio, 8);
    }

    @Test
    public void devuelveTrueCuandoComienzaConM()
    {
        String cadena = "mar del plata";
        boolean resultado = cadena.startsWith("m");
        Assert.assertTrue(resultado);
    }

    @Test
    public void devuelveFalseCuandoComienzaConM()
    {
        String cadena = "cordoba";
        boolean resultado = cadena.startsWith("m");
        Assert.assertFalse(resultado);
    }
}
