Base Project for TDD Workshop
=============================

![Build status](https://gitlab.com/fdrcbrtl/java-quickstart/badges/master/build.svg)

This is a base project for TDD workshop. It uses the following tools:

* JUnit
* Cucumber-JVM
* Cobertura

Commands:

* Compile and run tests: _mvn clean test_
* Check coverage: _mvn clean cobertura:cobertura_. The resulting report will be generated at target/site/cobertura/index.html.
